package com.marketplace.web.commands;

import com.marketplace.web.domain.Mp_Category;
import com.marketplace.web.domain.Mp_User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Arrays;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class ServiceCommand {


    private Long id_service;
    private String service_name;
    private Boolean status;
    private Float available_storge;
    private String  descripition;
    private Float price;
    private String  get_started;
    private Byte[] image;
    private Long id_category;
    private String relaseDate;
    private String idImage;

    public void setRelaseDate(String relaseDate) {
        this.relaseDate = relaseDate;
    }

    public String getIdImage() {
        return idImage;
    }

    public void setIdImage(String idImage) {
        this.idImage = idImage;
    }

    public void setAvailableDate(String availableDate) {
        this.availableDate = availableDate;
    }

    private String availableDate;

    private Long id_user;

    @Override
    public String toString() {
        return "ServiceCommand{" +
                "id_service=" + id_service +
                ", service_name='" + service_name + '\'' +
                ", status=" + status +
                ", available_storge=" + available_storge +
                ", descripition='" + descripition + '\'' +
                ", price=" + price +
                ", get_started='" + get_started + '\'' +
                ", image=" + Arrays.toString(image) +
                ", id_category=" + id_category +
                ", id_user=" + id_user +
                '}';
    }

    public Long getId_category() {
        return id_category;
    }

    public Long getId_service() {
        return id_service;
    }

    public void setId_service(Long id_service) {
        this.id_service = id_service;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Float getAvailable_storge() {
        return available_storge;
    }

    public void setAvailable_storge(Float available_storge) {
        this.available_storge = available_storge;
    }

    public String getDescripition() {
        return descripition;
    }

    public void setDescripition(String descripition) {
        this.descripition = descripition;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getGet_started() {
        return get_started;
    }

    public void setGet_started(String get_started) {
        this.get_started = get_started;
    }

    public Byte[] getImage() {
        return image;
    }

    public void setImage(Byte[] image) {
        this.image = image;
    }

    public void setId_category(Long id_category) {
        this.id_category = id_category;
    }

    public Long getId_user() {
        return id_user;
    }

    public void setId_user(Long id_user) {
        this.id_user = id_user;
    }

    public Boolean getStatus() {
        return status;
    }

    public String getRelaseDate() {
        return relaseDate;
    }

    public String getAvailableDate() {
        return availableDate;
    }
}
