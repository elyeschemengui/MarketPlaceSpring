package com.marketplace.web.commands;

import com.marketplace.web.domain.Mp_Service;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;
@Getter
@Setter
@NoArgsConstructor
public class CategoryCommand {


    private Long id_category;
    private String category_name;
    private Set<Mp_Service> mp_services = new HashSet<>();
    private Byte[] image;

    @Override
    public String toString() {
        return "CategoryCommand{" +
                "id_category=" + id_category +
                ", category_name='" + category_name + '\'' +
                ", mp_services=" + mp_services +
                '}';
    }

    public void setId_category(Long id_category) {
        this.id_category = id_category;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public Set<Mp_Service> getMp_services() {
        return mp_services;
    }

    public void setMp_services(Set<Mp_Service> mp_services) {
        this.mp_services = mp_services;
    }

    public Byte[] getImage() {
        return image;
    }

    public void setImage(Byte[] image) {
        this.image = image;
    }

    public Long getId_category() {
        return id_category;
    }
}
