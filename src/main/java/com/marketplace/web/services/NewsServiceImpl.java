package com.marketplace.web.services;

import com.marketplace.web.commands.CategoryCommand;
import com.marketplace.web.commands.NewCommand;
import com.marketplace.web.convertes.NewsCommandToNews;
import com.marketplace.web.convertes.NewsToNewsCommand;
import com.marketplace.web.domain.Mp_Category;
import com.marketplace.web.domain.News;
import com.marketplace.web.repositories.News_Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class NewsServiceImpl implements NewsService {

    private final News_Repository news_repository;
    private final NewsCommandToNews newsCommandToNews;
    private final NewsToNewsCommand newsToNewsCommand;
@Autowired
    public NewsServiceImpl(News_Repository news_repository, NewsCommandToNews newsCommandToNews, NewsToNewsCommand newsToNewsCommand) {
        this.news_repository = news_repository;
        this.newsCommandToNews = newsCommandToNews;
        this.newsToNewsCommand = newsToNewsCommand;
    }
    public News findById(Long id) {

        Optional<News> category = news_repository.findById(id);
        if (!category.isPresent()){
            throw new RuntimeException("Category not found");
        }
        return category.get();
    }
    @Override
    public NewCommand findCommandById(Long id) {
        NewCommand category =newsToNewsCommand.convert(findById(id));

        return newsToNewsCommand.convert(findById(id));
    }

    public Set<News> getCategory() {
        Set<News> categories = new HashSet<>();
        news_repository.findAll().iterator().forEachRemaining(categories::add);
        return categories;
    }

    @Override
    public NewCommand saveCategory(NewCommand news) {
        News category = newsCommandToNews.convert(news);
        News savedCategory = news_repository.save(category);

        return newsToNewsCommand.convert(savedCategory);
    }

    @Override
    public void saveImageFile(Long id, MultipartFile file) {
        try {
            News category = news_repository.findById(id).get();

            Byte[] byteObjects = new Byte[file.getBytes().length];
            int i=0;
            for (byte b: file.getBytes()){
                byteObjects[i++]=b;
            }
            category.setImage(byteObjects);
           news_repository.save(category);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
