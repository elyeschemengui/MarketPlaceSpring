package com.marketplace.web.services;

import com.marketplace.web.commands.CategoryCommand;
import com.marketplace.web.domain.Mp_Category;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;
import java.util.Set;

public interface CategoryService {


    CategoryCommand saveCategory(CategoryCommand command);

    public List<Mp_Category> searchC(String searchTermc);

    Set<Mp_Category> getCategory();
    void saveImageFile(Long categoryId,MultipartFile file);


    Mp_Category findById(Long id);
    CategoryCommand findCommandById(Long id);
    public void deleteCategory(Long id);
}
