package com.marketplace.web.services;

import com.marketplace.web.commands.CategoryCommand;
import com.marketplace.web.convertes.CategoryCommandToCategory;
import com.marketplace.web.convertes.CategoryToCategoryCommand;
import com.marketplace.web.domain.Mp_Category;
import com.marketplace.web.domain.Mp_Service;
import com.marketplace.web.repositories.Mp_Category_Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Service
public class CategoryServiceImpl implements CategoryService {
     private final CategoryCommandToCategory categoryCommandToCategory;
     private final CategoryToCategoryCommand categoryToCategoryCommand;
     private final Mp_Category_Repository mp_category_repository;

    public CategoryServiceImpl(CategoryCommandToCategory categoryCommandToCategory, CategoryToCategoryCommand categoryToCategoryCommand, Mp_Category_Repository mp_category_repository) {
        this.categoryCommandToCategory = categoryCommandToCategory;
        this.categoryToCategoryCommand = categoryToCategoryCommand;
        this.mp_category_repository = mp_category_repository;
    }

    @Override
    @Transactional
    public CategoryCommand saveCategory(CategoryCommand command) {
        Mp_Category category = categoryCommandToCategory.convert(command);
        Mp_Category savedCategory = mp_category_repository.save(category);

        return categoryToCategoryCommand.convert(savedCategory);
    }

    @Override
    public Set<Mp_Category> getCategory() {
        Set<Mp_Category> categories = new HashSet<>();
        mp_category_repository.findAll().iterator().forEachRemaining(categories::add);
        return categories;
    }

    @Override
    public List<Mp_Category> searchC(String searchTermc){
        List<Mp_Category> categories = new ArrayList<>();
        mp_category_repository.searchWithNativeQuery(searchTermc).iterator().forEachRemaining(categories::add);
        return categories;
    }

    @Override
    public void saveImageFile(Long categoryId, MultipartFile file) {
        try {
            Mp_Category category = mp_category_repository.findById(categoryId).get();

            Byte[] byteObjects = new Byte[file.getBytes().length];
            int i=0;
            for (byte b: file.getBytes()){
                byteObjects[i++]=b;
            }
            category.setImage(byteObjects);
            mp_category_repository.save(category);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Mp_Category findById(Long id) {

        Optional<Mp_Category> category = mp_category_repository.findById(id);
        if (!category.isPresent()){
            throw new RuntimeException("Category not found");
            }
            return category.get();
    }

    @Override
    public CategoryCommand findCommandById(Long id) {
        CategoryCommand category =categoryToCategoryCommand.convert(findById(id));

        return categoryToCategoryCommand.convert(findById(id));
    }

    @Override
    public void deleteCategory(Long id) {
        mp_category_repository.deleteById(id);
    }


}
