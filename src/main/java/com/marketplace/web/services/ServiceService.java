package com.marketplace.web.services;

import com.marketplace.web.commands.ServiceCommand;
import com.marketplace.web.domain.Mp_Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Set;

public interface ServiceService {

    ServiceCommand findByCategoryIdAndServiceId(Long categoryId,Long serviceId);
    ServiceCommand saveServiceCommand (ServiceCommand command);
    void saveImageFile(Long serviceId,MultipartFile file);
    Mp_Service findServiceById(Long serviceId);
    public List<Mp_Service> search(String searchTerm);
    public List<Mp_Service> newsService();
    void buyService(Long id,String idimage);
    void stopService();
    public List<Mp_Service> myService(Long id);
    public List<Mp_Service> listService();


    void deleteById(Long categoryId,Long idToDelete);

}
