package com.marketplace.web.services;


import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.model.PortBinding;
import com.marketplace.web.commands.ServiceCommand;
import com.marketplace.web.controllers.DockerController;
import com.marketplace.web.convertes.ServiceCommandToService;
import com.marketplace.web.convertes.ServiceToServiceCommand;
import com.marketplace.web.domain.Mp_Category;
import com.marketplace.web.domain.Mp_Service;
import com.marketplace.web.domain.Mp_User;
import com.marketplace.web.repositories.Mp_Category_Repository;
import com.marketplace.web.repositories.Mp_Service_Repository;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import com.github.dockerjava.api.DockerClient;

@Slf4j
@Service
public class ServiceServiceImpl implements ServiceService {

    private final Mp_Category_Repository mp_category_repository;
    private final Mp_Service_Repository mp_service_repository;
    private final DockerController dockerController;

    private final ServiceCommandToService serviceCommandToService;
    private final ServiceToServiceCommand serviceToServiceCommand;

    public ServiceServiceImpl(Mp_Category_Repository mp_category_repository,
                              Mp_Service_Repository mp_service_repository, DockerController dockerController, ServiceCommandToService serviceCommandToService,
                              ServiceToServiceCommand serviceToServiceCommand) {
        this.mp_category_repository = mp_category_repository;
        this.mp_service_repository = mp_service_repository;
        this.dockerController = dockerController;
        this.serviceCommandToService = serviceCommandToService;
        this.serviceToServiceCommand = serviceToServiceCommand;
    }

    @Override
    public ServiceCommand findByCategoryIdAndServiceId(Long categoryId, Long serviceId) {
        Optional<Mp_Category> categoryOptional = mp_category_repository.findById(categoryId);
        if (!categoryOptional.isPresent()){
            System.out.println("category not found");
        }
        Mp_Category category = categoryOptional.get();
        Optional<ServiceCommand> serviceCommand = category.getMp_services()
                .stream()
                .filter(service -> service.getId_service().equals(serviceId))
                .map(service -> serviceToServiceCommand.convert(service))
                .findFirst();
        if (!serviceCommand.isPresent()){
            System.out.println("service not found");
        }
        return serviceCommand.get();
    }

    @Override
    @Transactional
    public ServiceCommand saveServiceCommand(ServiceCommand command) {
        Optional<Mp_Category> categoryOptional = mp_category_repository.findById(command.getId_category());

        if (!categoryOptional.isPresent()){
            System.out.println("not found");
            return new ServiceCommand();
        }else {
            Mp_Category mp_category =categoryOptional.get();
            Optional<Mp_Service> serviceOptional =  mp_category.getMp_services()
                    .stream()
                    .filter(catyory->catyory.getId_service().equals(command.getId_service()))
                    .findFirst();

            if (serviceOptional.isPresent()){


                Mp_Service serviceFound =serviceOptional.get();

                serviceFound.setStatus(command.getStatus());
                serviceFound.setService_name(command.getService_name());
                serviceFound.setPrice(command.getPrice());
                serviceFound.setImage(command.getImage());
                serviceFound.setIdImage(command.getIdImage());
                serviceFound.setAvailableDate(command.getAvailableDate());
                serviceFound.setRelaseDate(command.getRelaseDate());
                serviceFound.setGet_started(command.getGet_started());
                serviceFound.setDescripition(command.getDescripition());
                serviceFound.setAvailable_storge(command.getAvailable_storge());
            }else{

                Mp_Service mp_service = serviceCommandToService.convert(command);
                mp_service.setMp_category(mp_category);
                mp_service.setRelaseDate(getCurrentDate());
                mp_category.addService(mp_service);
            }

            Mp_Category categorySaved =mp_category_repository.save(mp_category);

            Optional<Mp_Service> serviceOptional1Saved = categorySaved.getMp_services()
                    .stream()
                    .filter(categoryService->categoryService.getPrice().equals(command.getPrice()))
                    .findFirst();
            if (!serviceOptional1Saved.isPresent()){


                serviceOptional1Saved = mp_category.getMp_services()
                        .stream()
                        .filter(service -> service.getId_service().equals(command.getId_service()))
                        .filter(service -> service.getAvailable_storge().equals(command.getStatus()))
                        .filter(service -> service.getDescripition().equals(command.getDescripition()))
                        .filter(service -> service.getGet_started().equals(command.getDescripition()))
                        .filter(service -> service.getImage().equals(command.getImage()))
                        .filter(service -> service.getMp_category().getId_category().equals(command.getId_category()))
                        .filter(service -> service.getPrice().equals(command.getPrice()))
                        .filter(service -> service.getStatus().equals(command.getStatus()))
                        .findFirst();
            }



            return serviceToServiceCommand.convert(serviceOptional1Saved.get());

        }


    }

    @Override
    public void saveImageFile(Long serviceId, MultipartFile file) {
        try {
            Mp_Service service = mp_service_repository.findById(serviceId).get();

            Byte[] byteObjects = new Byte[file.getBytes().length];
            int i=0;
            for (byte b: file.getBytes()){
                byteObjects[i++]=b;
            }
            service.setImage(byteObjects);
            mp_service_repository.save(service);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Mp_Service findServiceById(Long serviceId) {
        Mp_Service service = mp_service_repository.findById(serviceId).get();

        return service;
    }

    @Override
    public List<Mp_Service> search(String searchTerm) {
        List<Mp_Service> services = new ArrayList<>();
        System.out.println(searchTerm);
        mp_service_repository.searchWithNativeQuery(searchTerm).iterator().forEachRemaining(services::add);
        System.out.println("lol");
        //services.forEach(item->System.out.println(services));
        return services;
    }

    @Override
    public List<Mp_Service> newsService() {
     List<Mp_Service> service = new ArrayList<>();
    return service;
    }

    @Autowired
    private JavaMailSender javaMailSender;

    @Override
    public void buyService(Long id,String idmage) {
        Mp_Service mp_service = mp_service_repository.findById(id).get();
        Mp_User user = new Mp_User();
        user.setId_user(Long.valueOf(1));
        mp_service.setUser(user);

        DockerClient client = dockerController.initDocker();
        CreateContainerResponse container = client.createContainerCmd(idmage)

                .withName("user-container")
                .withPortBindings(PortBinding.parse("9595:80"))
                .exec();


        mp_service_repository.save(mp_service);


        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);


        try {
            helper.setTo("mohamed.marrouchi@esprit.tn");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        try {
            helper.setSubject("Your new service");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        try {
            helper.setText("Adresse ip : 192.168.141.101 \n Port : 9595 \n Password : FkDlmD855**" );
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        client.startContainerCmd("user-container").exec();
        javaMailSender.send(message);

    }

    @Override
    public void stopService() {

        Set<Mp_Service> services = new HashSet<>();
        mp_service_repository.findAll().iterator().forEachRemaining(services::add);
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();

        for ( Mp_Service service : services
             ) {


            }
        }

    @Override
    public List<Mp_Service> myService(Long id) {
        List<Mp_Service> services = new ArrayList<>();
        mp_service_repository.findAll().stream().filter(x->x.getUser().getId_user().equals(id)).iterator().forEachRemaining(services::add);
        return services;
    }

    @Override
    public List<Mp_Service> listService() {
        List<Mp_Service> services = new ArrayList<>();
        mp_service_repository.findAll().iterator().forEachRemaining(services::add);
        return services;
    }


    @Override
    public void deleteById(Long categoryId, Long idToDelete) {




       Mp_Service mp_service =    mp_service_repository.findById(idToDelete).get();
       Mp_Category mp_category = mp_category_repository.findById(categoryId).get();
        mp_category.removeServices(mp_service);
        mp_service.setMp_category(null);
        System.out.println("here goooooooood");
        mp_category_repository.save(mp_category);
        mp_service_repository.delete(mp_service);

        }

public  String getCurrentDate(){
    Calendar cal = Calendar.getInstance();
    Date date=cal.getTime();
    DateFormat dateFormat = new SimpleDateFormat("dd-MM-YYYY");
    String formattedDate=dateFormat.format(date);
    return  formattedDate;
}




    }

