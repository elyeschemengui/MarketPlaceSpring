package com.marketplace.web.services;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Container;
import com.marketplace.web.controllers.DockerController;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
@Service
public class DockerServices {




    public List<Container>  getAllContainers(){

        DockerController dockerController = new DockerController();
        DockerClient client = dockerController.initDocker();
        List<Container> containers  = client.listContainersCmd()
                .withShowSize(true)
                .withShowAll(true)
                .withStatusFilter("exited").exec();


            return containers;
    }

}
