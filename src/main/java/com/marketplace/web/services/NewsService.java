package com.marketplace.web.services;

import com.marketplace.web.commands.CategoryCommand;
import com.marketplace.web.commands.NewCommand;
import com.marketplace.web.domain.News;
import org.springframework.web.multipart.MultipartFile;

public interface NewsService {


    NewCommand saveCategory(NewCommand news);
    void saveImageFile(Long id,MultipartFile file);
    NewCommand findCommandById(Long id);


}
