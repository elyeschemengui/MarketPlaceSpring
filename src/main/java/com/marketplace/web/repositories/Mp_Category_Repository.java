package com.marketplace.web.repositories;

import com.marketplace.web.domain.Mp_Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface Mp_Category_Repository extends JpaRepository<Mp_Category, Long> {

    @Query(
            value = "SELECT * FROM mp_category WHERE category_name LIKE %:searchTermc%",
            nativeQuery = true
    )
    public List<Mp_Category> searchWithNativeQuery(@Param("searchTermc") String searchTermc);

}
