package com.marketplace.web.repositories;

import com.marketplace.web.domain.Mp_User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;


public interface Mp_User_Repository extends JpaRepository<Mp_User, Long> {

}
