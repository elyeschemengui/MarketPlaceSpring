package com.marketplace.web.repositories;

import com.marketplace.web.domain.News;
import org.springframework.data.jpa.repository.JpaRepository;

public interface News_Repository extends JpaRepository<News, Long> {
}
