package com.marketplace.web.repositories;

import com.marketplace.web.domain.Mp_Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface Mp_Service_Repository extends JpaRepository<Mp_Service, Long> {

    @Query(
            value = "SELECT * FROM mp_service WHERE service_name LIKE %:searchTerm%",
            nativeQuery = true
    )
    public List<Mp_Service> searchWithNativeQuery(@Param("searchTerm") String searchTerm);




}
