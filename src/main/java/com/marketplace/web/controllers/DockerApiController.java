package com.marketplace.web.controllers;


import com.github.dockerjava.api.model.Container;
import com.marketplace.web.services.DockerServices;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping ("/api")
public class DockerApiController {


    private final DockerServices dockerservices;

    public DockerApiController(DockerServices dockerservices) {
        this.dockerservices = dockerservices;
    }


    @RequestMapping(value = "/containers/", method = RequestMethod.GET)
    public ResponseEntity<List<Container>> listAllContainers() {
        List<Container> containers = dockerservices.getAllContainers();

        return new ResponseEntity<List<Container>>(containers, HttpStatus.OK);
    }


}
