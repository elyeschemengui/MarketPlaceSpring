package com.marketplace.web.controllers.Users;

import com.marketplace.web.services.CategoryService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

public class CategoryController {

    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }


    @GetMapping("/categoriesu")
    public String showCategories(Model model){
        model.addAttribute("categoriesu",categoryService.getCategory());
        return "categuser.html";

    }


}
