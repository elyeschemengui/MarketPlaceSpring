package com.marketplace.web.controllers.Users;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.mail.internet.MimeMessage;

@Controller
public class contactController {



    @Autowired
    private JavaMailSender javaMailSender;

    @RequestMapping(value = "/contact",method = RequestMethod.POST)
    private void  sendEmail(@RequestParam("name") String name,@RequestParam("email") String email,@RequestParam("message") String body) throws Exception{
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setTo("greencode.marketplace@gmail.com");
        helper.setSubject(name+" his email address "+email);
        helper.setText("Message content : \n"+body);


        javaMailSender.send(message);


    }






}
