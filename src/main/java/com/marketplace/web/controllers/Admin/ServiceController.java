package com.marketplace.web.controllers.Admin;

import com.marketplace.web.commands.CategoryCommand;
import com.marketplace.web.commands.ServiceCommand;
import com.marketplace.web.repositories.Mp_Service_Repository;
import com.marketplace.web.services.CategoryService;
import com.marketplace.web.services.ServiceService;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.PathParam;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Slf4j
@Controller
public class ServiceController {
private final ServiceService serviceService;
private final CategoryService categoryService;
private final Mp_Service_Repository mp_service_repository;

    public ServiceController(ServiceService serviceService, CategoryService categoryService, Mp_Service_Repository mp_service_repository) {
        this.serviceService = serviceService;
        this.categoryService = categoryService;
        this.mp_service_repository = mp_service_repository;
    }

    @GetMapping("/admin/category/{id}/show")
    public String listService(@PathVariable String id, Model model){

         model.addAttribute("category",categoryService.findCommandById(Long.valueOf(id)));
         return "showcategory";
    }

    @GetMapping("/admin/category/{serviceId}/service/{categoryId}/update")
    public String updateService(@PathVariable String categoryId,
                                @PathVariable String serviceId,
                                Model model){
        model.addAttribute("service",serviceService.findByCategoryIdAndServiceId(Long.valueOf(categoryId),
                Long.valueOf(serviceId)));
        return "newservice";

    }



    @GetMapping("/service/{id}/new")
    public String newService(@PathVariable String id,Model model){


        CategoryCommand categoryCommand =categoryService.findCommandById(Long.valueOf(id));
        ServiceCommand serviceCommand = new ServiceCommand();
        serviceCommand.setId_category(Long.valueOf(id));
        model.addAttribute("service",serviceCommand);

        return "newservice";
    }

    @PostMapping("/admin/{id}/newservice")
    public String saveOrUpdata(@ModelAttribute ServiceCommand command,@RequestParam("imagefile")MultipartFile file){
        System.out.println(command);

        ServiceCommand serviceCommand = serviceService.saveServiceCommand(command);
        serviceService.saveImageFile(serviceCommand.getId_service(),file);
        return "redirect:/admin/categories";
    }

   // @RequestMapping(value = "/service",method = RequestMethod.POST)
    @PostMapping("/ResultSearch")
    public String searchservice(@RequestParam("name") String name, Model model){

        model.addAttribute("services",serviceService.search(name));
        return "searchservice";
    }

    @GetMapping("/myservice")
    public String myService(Model model){

        model.addAttribute("services",serviceService.myService(Long.valueOf(1)));
        return "myservice";
    }

    @GetMapping("/admin/category/{categoryId}/service/{serviceId}/delete")
    public String deleteService(@PathVariable String categoryId,
                                @PathVariable String serviceId){
        serviceService.deleteById(Long.valueOf(categoryId),Long.valueOf(serviceId));
        mp_service_repository.deleteById(Long.valueOf(serviceId));
       // mp_service_repository.deleteservice(Long.valueOf(serviceId));
        return "redirect:/admin/category/"+categoryId+"/show";
    }

    @GetMapping("/buyservice/{id}/{idimage}")
    public String buyService(@PathVariable String id,@PathVariable String idimage){

        serviceService.buyService(Long.valueOf(id),idimage);
        return "redirect:/admin/categories";
    }

    @GetMapping("/listservice")
    public String listService (Model model){

        model.addAttribute("services",serviceService.listService());
        return "listservice";

    }




    @GetMapping("/admin/category/{categoryId}/service/{serviceId}/recipeimage")
    public void renderImageFromDB(@PathVariable String categoryId,@PathVariable String serviceId, HttpServletResponse response) throws IOException {

        ServiceCommand serviceCommand = serviceService.findByCategoryIdAndServiceId(Long.valueOf(categoryId),Long.valueOf(serviceId));
        System.out.println("hhhhhhhhhhh"+serviceCommand);

        byte[] bytes = new byte[serviceCommand.getImage().length];
        int i =0;
        for (byte b : serviceCommand.getImage()){
            bytes[i++]=b;
        }

        response.setContentType("image/jpeg");

        InputStream is = new ByteArrayInputStream(bytes);
        IOUtils.copy(is,response.getOutputStream());
    }

    @GetMapping("/service/{serviceId}/show")
    public String showService(@PathVariable String serviceId,Model model){
        model.addAttribute("service",serviceService.findServiceById(Long.valueOf(serviceId)));
        model.addAttribute("news",serviceService.newsService());


        return "detailscategory";
    }


}
