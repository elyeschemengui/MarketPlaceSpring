package com.marketplace.web.controllers.Admin;

import com.marketplace.web.commands.NewCommand;
import com.marketplace.web.domain.News;
import com.marketplace.web.repositories.News_Repository;
import com.marketplace.web.services.NewsService;
import com.marketplace.web.services.NewsServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Controller

public class NewsController {

    private  final NewsService newsService;
    private final NewsServiceImpl newsServiceimp;
    private final News_Repository news_repository;

    public NewsController(NewsService newsService, NewsServiceImpl newsServiceimp, News_Repository news_repository) {
        this.newsService = newsService;
        this.newsServiceimp = newsServiceimp;
        this.news_repository = news_repository;
    }


    @GetMapping("new")
    public String newCategory(Model model){

        model.addAttribute("news",new News());


        return "news";
    }

    @GetMapping("/admin/new/new")
    public String showCategory(Model model){
        model.addAttribute("news",newsServiceimp.getCategory());
        return "listnew";

    }
    @GetMapping("/admin/new/{id}/delete")
    public String updateCategory(@PathVariable String id, Model model){
        news_repository.deleteById(Long.valueOf(id));
        return "/admin/new/new";
    }

    @PostMapping("/admin/new")
    public String saveOrUpdate(@ModelAttribute NewCommand news, @RequestParam("imagefile")MultipartFile file){

        NewCommand categoryCommand = newsService.saveCategory(news);
        newsService.saveImageFile(categoryCommand.getId(),file);

        return "redirect:/admin/categories";
    }

}
