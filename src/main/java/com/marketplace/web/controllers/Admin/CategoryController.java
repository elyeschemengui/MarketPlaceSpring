package com.marketplace.web.controllers.Admin;

import com.marketplace.web.commands.CategoryCommand;
import com.marketplace.web.commands.ServiceCommand;
import com.marketplace.web.services.CategoryService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Controller
public class CategoryController {


    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }


    @GetMapping("/admin/newcategory")
    public String newCategory(Model model){

        model.addAttribute("category",new CategoryCommand());


        return "newcategory";
    }


    @GetMapping("/admin/category/{id}/update")
    public String updateCategory(@PathVariable String id, Model model){
        model.addAttribute("category",categoryService.findCommandById(Long.valueOf(id)));
        return "newcategory";
    }

    @PostMapping("/admin/newcategory")
    public String saveOrUpdate(@ModelAttribute CategoryCommand command,@RequestParam("imagefile")MultipartFile file){

        CategoryCommand categoryCommand = categoryService.saveCategory(command);
        categoryService.saveImageFile(categoryCommand.getId_category(),file);

        return "redirect:/admin/categories";
    }

    @GetMapping("/admin/categories")
    public String showCategory(Model model){
        model.addAttribute("categories",categoryService.getCategory());
        return "listcategory";

    }

    @PostMapping("/ResultSearchc")
    public String searchcategory(@RequestParam("name") String name,Model model){
        model.addAttribute("categories",categoryService.searchC(name));
        return "searchcategory";
    }

    @GetMapping("/admin/category/{id}/delete")
    public String deleteCategory (@PathVariable String id){
        categoryService.deleteCategory(Long.valueOf(id));
        return "redirect:/admin/categories";
    }

    @GetMapping("/admin/category/{categoryId}/recipeimage")
    public void renderImageFromDB(@PathVariable String categoryId, HttpServletResponse response) throws IOException {

        CategoryCommand categoryCommand = categoryService.findCommandById(Long.valueOf(categoryId));

        byte[] bytes = new byte[categoryCommand.getImage().length];
        int i =0;
        for (byte b : categoryCommand.getImage()){
            bytes[i++]=b;
        }

        response.setContentType("image/jpeg");

        InputStream is = new ByteArrayInputStream(bytes);
        IOUtils.copy(is,response.getOutputStream());
    }



}
