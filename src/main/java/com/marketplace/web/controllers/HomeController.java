package com.marketplace.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {


    @GetMapping("/register")
    public String registerAction(){
        return "signin-signup";
    }

    @GetMapping("/contact")
    public String contactAction(){
        return "contact";
    }
}
