package com.marketplace.web.controllers;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.InspectContainerResponse;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.core.DockerClientBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public class DockerController {
    public DockerClient initDocker() {
        return DockerClientBuilder.getInstance("tcp://localhost:4243").build();
    }

    @GetMapping("/containers")
    public String manageContainersAction(Model model){
        DockerClient client = initDocker();
        List<Container> containers  = client.listContainersCmd()
                .withShowSize(true)
                .withShowAll(true)
                .withStatusFilter("exited").exec();

        List<Container> containersUp  = client.listContainersCmd()
                .exec();


        model.addAttribute("containers",containers);
        model.addAttribute("containersUp",containersUp);


        return "manageContainers";

    }




    @GetMapping("/container/delete/{id}")
    public String deleteContainer(@PathVariable String id) {
        DockerClient client = initDocker();
        client.removeContainerCmd(id).exec();
        return "redirect:/containers";
    }
    @GetMapping("/container/start/{id}")
    public String startContainer(@PathVariable String id) {
        DockerClient client = initDocker();
        client.startContainerCmd(id).exec();
        return "redirect:/containers";
    }
    @GetMapping("/container/stop/{id}")
    public String stopContainer(@PathVariable String id) {
        DockerClient client = initDocker();
        client.stopContainerCmd(id).exec();
        return "redirect:/containers";
    }
    @GetMapping("/container/show/{id}")
    public String showContainer(@PathVariable String id,Model model) {
        DockerClient client = initDocker();
        InspectContainerResponse container  = client.inspectContainerCmd(id).exec();
        model.addAttribute("container",container);
        return "showContainers";
    }
}
