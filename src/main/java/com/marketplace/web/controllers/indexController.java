package com.marketplace.web.controllers;

import com.marketplace.web.services.CategoryService;
import com.marketplace.web.services.ServiceService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class indexController {
    private final CategoryService categoryService;
    private final ServiceService serviceService;

    public indexController(CategoryService categoryService, ServiceService serviceService) {
        this.categoryService = categoryService;
        this.serviceService = serviceService;
    }

    @GetMapping("/")
    public String index (Model model){

        model.addAttribute("categories",categoryService.getCategory());

        return "index";


}




}
