package com.marketplace.web.domain;

import javax.persistence.*;

@Entity
public class News {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Lob
    private Byte[] image;
    @Lob
    private String newss;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Byte[] getImage() {
        return image;
    }

    public void setImage(Byte[] image) {
        this.image = image;
    }

    public String getNews() {
        return newss;
    }

    public void setNews(String news) {
        this.newss = news;
    }
}
