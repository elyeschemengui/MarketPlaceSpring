package com.marketplace.web.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@EqualsAndHashCode(exclude = {"mp_services"})
@Data
public class Mp_Category {


    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id_category;
    private String category_name;
    @Lob
    private Byte[] image;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mp_category", orphanRemoval = true)
    private Set<Mp_Service> mp_services = new HashSet<>();

    public Mp_Category addService(Mp_Service service){
        service.setMp_category(this);
        this.mp_services.add(service);
        return this;
    }

    public Long getId_category() {
        return id_category;
    }
    public void removeServices(Mp_Service mp_service) {
        mp_services.remove(mp_service);
    }

    public void setId_category(Long id_category) {
        this.id_category = id_category;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public void setMp_services(Set<Mp_Service> mp_services) {
        this.mp_services = mp_services;
    }

    public Set<Mp_Service> getMp_services() {
        return mp_services;
    }

    public Byte[] getImage() {
        return image;
    }

    public void setImage(Byte[] image) {
        this.image = image;
    }
}
