package com.marketplace.web.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Date;
@Data
@Entity
public class Mp_Service {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id_service;
	private String service_name;
	private Boolean status = false;
	private Float available_storge;
	private String idImage;
	@Lob
	private String  descripition;
	private Float price;
	@Lob
	private String  get_started;
	@Lob
	private Byte[] image;
	@ManyToOne
    @JoinColumn(name = "id_category")
	private Mp_Category mp_category;
	private String availableDate;
    private String relaseDate;
	@ManyToOne
	@JoinColumn(name="id_user",foreignKey=@ForeignKey(name="id_user_fk"))
	private Mp_User user;


	public void setMp_category(Mp_Category mp_category) {
		this.mp_category = mp_category;
	}

	public void setId_service(Long id_service) {
		this.id_service = id_service;
	}

	public String getService_name() {
		return service_name;
	}

	public void setService_name(String service_name) {
		this.service_name = service_name;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Float getAvailable_storge() {
		return available_storge;
	}

	public void setAvailable_storge(Float available_storge) {
		this.available_storge = available_storge;
	}

	public String getDescripition() {
		return descripition;
	}

	public void setDescripition(String descripition) {
		this.descripition = descripition;
	}

	public Float getPrice() {
		return price;
	}

	public String getIdImage() {
		return idImage;
	}

	public void setIdImage(String idImage) {
		this.idImage = idImage;
	}

	public String getAvailableDate() {
		return availableDate;
	}


	public String getRelaseDate() {
		return relaseDate;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public String getGet_started() {
		return get_started;
	}

	public void setGet_started(String get_started) {
		this.get_started = get_started;
	}

	public Byte[] getImage() {
		return image;
	}

	public void setImage(Byte[] image) {
		this.image = image;
	}

	public Mp_Category getMp_category() {
		return mp_category;
	}

	public Mp_User getUser() {
		return user;
	}

	public void setUser(Mp_User user) {
		this.user = user;
	}

	public Long getId_service() {
		return id_service;
	}

	public void setRelaseDate(String relaseDat) {
		this.relaseDate = relaseDat;
	}

    public void setAvailableDate(String availableDate) {
        this.availableDate = availableDate;
    }

    @Override
	public String toString() {
		return "Mp_Service{" +
				"id_service=" + id_service +
				", service_name='" + service_name + '\'' +
				", status=" + status +
				", available_storge=" + available_storge +
				", descripition='" + descripition + '\'' +
				", price=" + price +
				", get_started='" + get_started + '\'' +
				", image=" + Arrays.toString(image) +
				", mp_category=" + mp_category +
				", availableDate='" + availableDate + '\'' +
				", relaseDate='" + relaseDate + '\'' +
				", user=" + user ;
				};


}

