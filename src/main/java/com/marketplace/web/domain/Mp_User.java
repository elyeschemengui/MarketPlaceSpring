package com.marketplace.web.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@EqualsAndHashCode(exclude = {"mp_services"})

@Entity
public class Mp_User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id_user;
	private String first_name;
	private String last_name;
	private String mail;
	private String password;
	private Date birthdate;
	private String country;
	private String zip;
	private int tel;
	private Boolean status=false;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user",fetch = FetchType.EAGER)
	private Set<Mp_Service> mp_services = new HashSet<>();
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles;
	public Mp_User() {
		super();
	}

	public Mp_User addService(Mp_Service service){
		service.setUser(this);
		this.mp_services.add(service);
		return this;
	}


	public Mp_User(String first_name, String last_name, String mail, String password, Date birthdate, String country,
                   String zip, int tel, Boolean status) {
		super();
		this.first_name = first_name;
		this.last_name = last_name;
		this.mail = mail;
		this.password = password;
		this.birthdate = birthdate;
		this.country = country;
		this.zip = zip;
		this.tel = tel;
		this.status = status;
	}

	public Long getId_user() {
		return id_user;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public int getTel() {
		return tel;
	}

	public void setTel(int tel) {
		this.tel = tel;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Set<Mp_Service> getMp_services() {
		return mp_services;
	}

	public void setMp_services(Set<Mp_Service> mp_services) {
		this.mp_services = mp_services;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public void setId_user(Long id_user) {
        this.id_user = id_user;
    }
}
