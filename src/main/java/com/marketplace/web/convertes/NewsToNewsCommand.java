package com.marketplace.web.convertes;

import com.marketplace.web.commands.NewCommand;
import com.marketplace.web.domain.News;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class NewsToNewsCommand implements Converter<News,NewCommand> {
    @Override
    public NewCommand convert(News news) {
        final NewCommand newCommand = new NewCommand();
        newCommand.setId(news.getId());
        newCommand.setNews(news.getNews());
        newCommand.setTile(news.getNews());
        newCommand.setImage(news.getImage());

        return newCommand;
    }
}
