package com.marketplace.web.convertes;

import com.marketplace.web.commands.ServiceCommand;
import com.marketplace.web.domain.Mp_Category;
import com.marketplace.web.domain.Mp_Service;
import com.marketplace.web.domain.Mp_User;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class ServiceCommandToService implements Converter<ServiceCommand,Mp_Service> {


    @Override
    @Nullable
    @Synchronized
    public Mp_Service convert(ServiceCommand source) {
        if (source == null){
        return null;}
        final Mp_Service mp_service = new Mp_Service();
        mp_service.setAvailable_storge(source.getAvailable_storge());
        mp_service.setDescripition(source.getDescripition());
        mp_service.setGet_started(source.getGet_started());
        mp_service.setId_service(source.getId_service());
        mp_service.setImage(source.getImage());
        mp_service.setPrice(source.getPrice());
        mp_service.setService_name(source.getService_name());
        mp_service.setAvailableDate(source.getAvailableDate());
        mp_service.setRelaseDate(source.getRelaseDate());
        mp_service.setStatus(source.getStatus());
        if (source.getId_category() !=null){
            Mp_Category mp_category = new Mp_Category();

            mp_category.setId_category(source.getId_category());
            mp_service.setMp_category(mp_category);
            mp_category.addService(mp_service);
            }
        if (source.getId_user() !=null){
            Mp_User mp_user = new Mp_User();

            mp_user.setId_user(source.getId_user());
            mp_service.setUser(mp_user);
            mp_user.addService(mp_service);
        }
        return mp_service;

    }
}
