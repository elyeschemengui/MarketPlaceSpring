package com.marketplace.web.convertes;

import com.marketplace.web.commands.CategoryCommand;
import com.marketplace.web.domain.Mp_Category;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class CategoryToCategoryCommand implements Converter<Mp_Category,CategoryCommand> {
    @Override
    @Nullable
    @Synchronized
    public CategoryCommand convert(Mp_Category source) {
        if (source == null) {
            return null;
        }

        final CategoryCommand categoryCommand = new CategoryCommand();
        categoryCommand.setCategory_name(source.getCategory_name());
        categoryCommand.setId_category(source.getId_category());
        categoryCommand.setMp_services(source.getMp_services());
        categoryCommand.setImage(source.getImage());
        return categoryCommand;
    }
}
