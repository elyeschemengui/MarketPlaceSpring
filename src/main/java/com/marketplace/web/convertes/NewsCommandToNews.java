package com.marketplace.web.convertes;

import com.marketplace.web.commands.CategoryCommand;
import com.marketplace.web.commands.NewCommand;
import com.marketplace.web.domain.Mp_Category;
import com.marketplace.web.domain.News;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class NewsCommandToNews implements Converter<NewCommand,News> {
    @Override
    public News convert(NewCommand newCommand) {

         final News news = new News();
         news.setImage(newCommand.getImage());
         news.setId(newCommand.getId());
         news.setNews(newCommand.getNews());
         news.setTitle(newCommand.getTile());

         return news;


    }
}
