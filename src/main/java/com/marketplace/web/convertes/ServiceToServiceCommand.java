package com.marketplace.web.convertes;

import com.marketplace.web.commands.ServiceCommand;
import com.marketplace.web.domain.Mp_Service;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class ServiceToServiceCommand implements Converter<Mp_Service,ServiceCommand> {
    @Override
    @Nullable
    @Synchronized
    public ServiceCommand convert(Mp_Service source) {
        if (source==null){
        return null;}
        final ServiceCommand serviceCommand = new ServiceCommand();

        serviceCommand.setAvailable_storge(source.getAvailable_storge());
        serviceCommand.setDescripition(source.getDescripition());
        serviceCommand.setGet_started(source.getGet_started());
        serviceCommand.setId_service(source.getId_service());
        serviceCommand.setImage(source.getImage());
        serviceCommand.setPrice(source.getPrice());
        serviceCommand.setService_name(source.getService_name());
        serviceCommand.setStatus(source.getStatus());
        serviceCommand.setRelaseDate(source.getRelaseDate());
        serviceCommand.setAvailableDate(source.getAvailableDate());
        serviceCommand.setId_category(source.getMp_category().getId_category());
        return serviceCommand;

    }
}
