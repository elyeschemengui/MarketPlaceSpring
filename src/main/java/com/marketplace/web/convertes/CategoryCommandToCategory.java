package com.marketplace.web.convertes;

import com.marketplace.web.commands.CategoryCommand;
import com.marketplace.web.domain.Mp_Category;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class CategoryCommandToCategory implements Converter <CategoryCommand,Mp_Category> {
    @Override
    @Nullable
    @Synchronized
    public Mp_Category convert(CategoryCommand source) {

        if (source == null){
            return null;
        }

        final Mp_Category mp_category = new Mp_Category();
        mp_category.setCategory_name(source.getCategory_name());
        mp_category.setId_category(source.getId_category());
        mp_category.setMp_services(source.getMp_services());
        mp_category.setImage(source.getImage());


        return mp_category;
    }
}
